<?php /*========================================
other
================================================*/ ?>
<div class="c-dev-title1">other</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-label1</div>
<div class="c-label1">
	<div class="c-label1__block">
		<div class="c-label1__lab">
			レッスン・フリー利用可能時間
		</div>
		<div class="c-label1__txt">
			1時間（準備時間含む、会員・ビジター問わず）
		</div>
	</div>
	<div class="c-label1__block">
		<div class="c-label1__lab c-label1__lab--ceru">
			学生割引
		</div>
		<div class="c-label1__txt">
			上記料金より50％OFF（会員カテゴリのみ適用※フリー練習含まず）
		</div>
	</div>
</div>


<?php $pageid="index";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>
<div class="l-container">
	<table class="c-table1">
		<caption>通常料金 ※(税抜)</caption>
		<thead>
			<tr>
				<th colspan="2">会員</th>
				<th>ビジター</th>
				<th>フリー練習費</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>⼊会⾦</td>
				<td>￥5,000／1回</td>
				<td><span>−</span></td>
				<td><span>−</span></td>
			</tr>
			<tr>
				<td><p>レギュラー </p><p>全⽇利⽤可</p></td>
				<td>￥10,000（税抜）／⽉額</td>
				<td><span>−</span></td>
				<td><span class="u-red">無料</span></td>
			</tr>

			<tr>
				<td><p>デイタイム </p><p>平⽇昼限定（11:00〜17:00）</p></td>
				<td>￥7,000（税抜）／⽉額</td>
				<td><span>−</span></td>
				<td>時間外有料</td>
			</tr>
			<tr>
				<td><p>ジュニア </p><p>⽊曜⽇限定 17：00〜18：00<br>中学3年生まで</p></td>
				<td>￥6,000（税抜）／⽉額</td>
				<td><span>−</span></td>
				<td>時間外有料</td>
			</tr>
			<tr>
				<td><p>ホリデイ </p><p>⼟・⽇・祝のみ終⽇利⽤可</p></td>
				<td>￥8,000（税抜）／⽉額</td>
				<td><span>−</span></td>
				<td>時間外有料</td>
			</tr>
			<tr>
				<td><p>レッスン4 </p><p>レッスン4回<br>(スタンプカード制／有効期間：3ヶ月）</p></td>
				<td><span>−</span></td>
				<td>￥8,000（税抜）／1セット</td>
				<td>有料</td>
			</tr>
			<tr>
				<td><p>フリー練習 </p><p>空打席があれば利⽤可<br>※レッスンは含まず</p></td>
				<td><span class="u-break">￥1,000（税抜）／1回</span><br><span class="u-red">プレオープン期間限定 ￥0</span></td>
				<td><span class="u-break">￥1,500（税抜）／1回</span><br><span class="u-red">プレオープン期間限定 ￥0</span></td>
				<td>有料</td>
			</tr>
			<tr>
				<td><p>体験レッスン </p><p>全⽇利⽤可</p></td>
				<td><span>−</span></td>
				<td>￥2,000（税抜）／1回</td>
				<td><span>−</span></td>
			</tr>
		</tbody>
	</table>
	<div class="c-label1">
		<div class="c-label1__block">
			<div class="c-label1__lab">
				レッスン・フリー利用可能時間
			</div>
			<div class="c-label1__txt">
				1時間（準備時間含む、会員・ビジター問わず）
			</div>
		</div>
		<div class="c-label1__block">
			<div class="c-label1__lab c-label1__lab--ceru">
				学生割引
			</div>
			<div class="c-label1__txt">
				上記料金より50％OFF（会員カテゴリのみ適用※フリー練習含まず）
			</div>
		</div>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>
